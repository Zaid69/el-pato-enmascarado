/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.AutoDao;
import Model.AutoModel;
import com.oreilly.servlet.MultipartRequest;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Ahorra_HP
 */
public class AutoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        String strRespuesta = "";
        //Decodificamos la cadena que llego
        String cadenaOrg = request.getParameter("loadSearch");
        //Separamos la cadena que se ha decodificado, mediante ele separador |
        String cadena[] = cadenaOrg.split("\\|");
        //asignamos el valor a tipo
        String tipo = cadena[0];
        PrintWriter out = response.getWriter();
        String jsonString = "";
        if (isMultipart) {
            strRespuesta = UploadDoc(request);
            response.sendRedirect(strRespuesta);
        } else {
            if (tipo.equals("1")) {
                try {
                    //Enviamos la petición al Dao para buscar el parametro que recibimos
                    //y se guarda el pm el resultado
                    AutoModel[] am = new AutoDao().searchAuto(cadena[1]);
                    JSONArray arAutos = new JSONArray();
                    //recorremos el resultado del query para asignarlo al JSONArray
                    for (int i = 0; i < am.length; i++) {
                        AutoModel aModel = am[i];
                        //Se declara el JSONObject para asiganrlo al JSONArray
                        JSONObject objeto = new JSONObject();
                        objeto.put("id", "" + aModel.getIdAuto());
                        objeto.put("img", "" + aModel.getImgAuto());
                        objeto.put("marca", "" + aModel.getMarcaAuto());
                        objeto.put("ano", "" + aModel.getAñoAuto());
                        objeto.put("modelo", "" + aModel.getModelAuto());
                        objeto.put("cat", "" + aModel.getCategAuto());
                        objeto.put("km", "" + aModel.getKmAuto());
                        objeto.put("trans", "" + aModel.getTransAuto());
                        objeto.put("precio", "" + aModel.getPrecio());
                        //se asigana el JSONObject al JSONArray
                        arAutos.add(objeto);
                    }
                    //Se asigna el JSONArray que contiene varios JSONObjects al 
                    //JSONObject padre
                    JSONObject objPadre = new JSONObject();
                    objPadre.put("autos", arAutos);
                    jsonString = objPadre.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    out.write(jsonString);
                    out.close();
                }
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public AutoModel getAutoById(String autoId) {
        return new AutoDao().getAutoById(autoId);
    }

    public AutoModel[] getAutoModels() {
        return new AutoDao().getAutoModel();
    }

    private String UploadDoc(HttpServletRequest request) throws IOException {
        ServletContext context = getServletContext();
        boolean success = false;
        boolean hasfile = false;
        String strRuta = context.getRealPath(request.getServletPath());
        String strRutaProd = context.getRealPath(request.getServletPath());
        String strRutaFinal = "";
        if (strRuta.lastIndexOf("\\") > 0) {
            strRuta = strRuta.substring(0, strRuta.lastIndexOf("\\"));
            strRutaFinal = strRuta;
            strRuta += "\\upload\\";
        } else {
            strRuta = strRuta.substring(0, strRuta.lastIndexOf("/"));
            strRutaFinal = strRuta;
            strRuta += "/upload/";
        }
        File f = new File(strRuta);
        if (f.isDirectory() == false) {
            success = f.mkdir();
        }
        //Ejecuta métodos establece el tamanio maximo de la imagen que puedo recibir
        MultipartRequest mRequest = new MultipartRequest(request, strRuta, 1150000);
        String hidCallback = "";
        String mary = mRequest.getParameter("Mary");

        Enumeration objArchivos = mRequest.getFileNames();
        String strArchivo = "";
        String strArchivoO = "";
        String strExt = "";
        boolean blnEstatus = false;
        String error = "0";
        while (objArchivos.hasMoreElements()) {
            strArchivo = (String) objArchivos.nextElement();
            strArchivoO = mRequest.getOriginalFileName(strArchivo);
            strExt = FilenameUtils.getExtension(strArchivoO);
            if (strArchivoO != null) {
                hasfile = true;
            }
        }//while

        String strFileUploaded = "";
        if (strArchivoO != null) {
            if (strRuta.lastIndexOf("\\") > 0) {
                strFileUploaded = strRuta + strArchivoO;
            } else {
                strFileUploaded = strRuta + strArchivoO;
            }
        }

        String txtMarca = new String(mRequest.getParameter("marca").getBytes("iso-8859-1"), "UTF-8").trim();
        String txtAno = new String(mRequest.getParameter("ano").getBytes("iso-8859-1"), "UTF-8").trim();
        String txtModelo = new String(mRequest.getParameter("modelo").getBytes("iso-8859-1"), "UTF-8").trim();
        String txtCategoria = new String(mRequest.getParameter("categoria").getBytes("iso-8859-1"), "UTF-8").trim();
        String txtKm = new String(mRequest.getParameter("km").getBytes("iso-8859-1"), "UTF-8").trim();
        String txtTransmision = new String(mRequest.getParameter("transmision").getBytes("iso-8859-1"), "UTF-8").trim();
        String txtPrecio = new String(mRequest.getParameter("precio").getBytes("iso-8859-1"), "UTF-8").trim();

        AutoModel am = new AutoModel();

        String idAuto = mRequest.getParameter("idAuto");
        if (idAuto == null) {
            idAuto = "";
        }

        if (mary.equalsIgnoreCase("EditAuto")) {
            am.setIdAuto(idAuto);
            am.setMarcaAuto(txtMarca);
            am.setAñoAuto(txtAno);
            am.setModelAuto(txtModelo);
            am.setCategAuto(txtCategoria);
            am.setKmAuto(txtKm);
            am.setTransAuto(txtTransmision);
            am.setPrecio(txtPrecio);

            AutoModel amConImg = new AutoDao().getAutoById(idAuto);
            blnEstatus = new AutoDao().updateAuto(am);
            String archivoBorrar = "";
            if (strRutaProd.lastIndexOf("\\") > 0) {
                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("\\"));
                strRutaProd += "\\";
                archivoBorrar = strRutaProd + amConImg.getImgAuto();
            } else {
                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("/"));
                strRutaProd += "/";
            }
//            String archivoBorrar = "";
//            if (strRutaProd.lastIndexOf("\\") > 0) {
//                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("\\"));
//                strRutaProd += "\\";
//                archivoBorrar = strRutaProd + amConImg.getImgAuto();
//                archivoBorrar = archivoBorrar.replaceAll("\\", "\\\\");
//            } else {
//                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("/"));
//                strRutaProd += "/";
//                archivoBorrar = strRutaProd + amConImg.getImgAuto();
//            }
            if (hasfile) {
                blnEstatus = new AutoDao().img(idAuto, strFileUploaded, strRutaFinal, strExt, archivoBorrar);
            }
            hidCallback = "dashboard/formAutos.jsp?id=" + idAuto;
        }
        if (mary.equalsIgnoreCase("nuevoAuto")) {

            am.setIdAuto(idAuto);
            am.setMarcaAuto(txtMarca);
            am.setAñoAuto(txtAno);
            am.setModelAuto(txtModelo);
            am.setCategAuto(txtCategoria);
            am.setKmAuto(txtKm);
            am.setTransAuto(txtTransmision);
            am.setPrecio(txtPrecio);

            idAuto = new AutoDao().newAuto(am);
            String archivoBorrar = "";
            if (strRutaProd.lastIndexOf("\\") > 0) {
                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("\\"));
                strRutaProd += "\\";
            } else {
                strRutaProd = strRutaProd.substring(0, strRutaProd.lastIndexOf("/"));
                strRutaProd += "/";
            }
            if (hasfile) {
                blnEstatus = new AutoDao().img(idAuto, strFileUploaded, strRutaFinal, strExt, archivoBorrar);
            }
            hidCallback = "dashboard/formAutos.jsp?Id=" + idAuto;
        }
        return hidCallback;
    }
}
