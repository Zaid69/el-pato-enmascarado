/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.TituloDao;
import Model.TituloModel;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Zaid
 */
public class TituloController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String jsonString = "";
        JSONParser parser = new JSONParser();
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
            System.out.println("Form tipo multipart");
        }
        try {
            String M = request.getParameter("Mary");
            if (null != request.getParameter("Mary")) {
                try {
                    if (M.equals("tituloEdit")) {
                        TituloModel tm = new TituloModel();
                        tm.setNo(request.getParameter("tituloId"));
                        tm.setTitulo(request.getParameter("Titulo"));
                        tm.setSubTitulo(request.getParameter("SubTitulo"));
                        tm.setTitulo2(request.getParameter("Titulo2"));
                        tm.setSubTitulo2(request.getParameter("SubTitulo2"));
                        tm.setTitulo3(request.getParameter("Titulo3"));
                        tm.setCotenido(request.getParameter("Contenido"));
                        tm.setTitulo4(request.getParameter("Titulo4"));
                        tm.setCotenido2(request.getParameter("Contenido2"));
                        tm.setTitulo5(request.getParameter("Titulo5"));
                        tm.setCotenido3(request.getParameter("Contenido3"));
                        boolean resEdit = new TituloDao().updateTituloModel(tm);
                        response.sendRedirect("dashboard/formTitulo.jsp?Id=" + tm.getNo() + "&res=" + resEdit);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public TituloModel[] getTituloModels() {
        TituloModel[] tituloModels = new TituloDao().getTituloModel();
        return tituloModels;
    }

    public TituloModel getModelById(String tituloId) {
        TituloModel tituloModel = new TituloDao().getTituloById(tituloId);
        return tituloModel;
    }

}
