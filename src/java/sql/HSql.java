/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Properties;
import utilities.Parametros;


/**
 *
 * @author tlacaelel21
 */
public class HSql {

    private static Properties datos = new Properties();
    private static String _driver = "";
    private static String _connectString = "";
    private static String _password = "";
    private static String _user = "";
    private static String _debug = "1";

    public void HSql() {
        try {
            //String path = "C:\\Users\\Ivan\\Documents\\NetBeansProjects\\Cptm\\web\\WEB-INF";
            //String path = "D:\\xampp\\tomcat\\webapps\\Cptm\\WEB-INF";

            if (_user.equals("")) {
                Parametros parametros = new Parametros();
                _connectString = parametros.ReadParameter("dbconnection", null);
                _user = parametros.ReadParameter("dbuser", null);
                _password = parametros.ReadParameter("dbpassword", null);
                _driver = parametros.ReadParameter("driver", null);
                /*
                        datos.load(new FileInputStream(path+ "/sql.properties"));
                       _connectString= datos.getProperty("connectString");
                       _user=datos.getProperty("user");
                       _password=datos.getProperty("password");
                       _driver=datos.getProperty("driver");
                 */
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean ComandoConsola(String strSQL) {
        HSql sql = new HSql();
        HashMap[] arrResultados = sql.ExecuteSQL(strSQL);
        HashMap resultado = (HashMap) arrResultados[0];
        String successValue = (String) resultado.get("success");
        if (successValue.equals("0")) {
            return false;
        } else {
            return true;
        }
    }

    public HashMap[] QuerySQL(String strSQL) {

        HSql();
        String debugmode = _debug;
        if (debugmode.equals("1")) {
            System.out.println(strSQL);
        }

        //1. Abrir conexión (reciclar de connection pool)
        //2. Realizar consulta
        //3. Recorrer recordset, creando un hashtable por registro
        //3.1 Cada campo será un elemento dentro de la tabla
        //4. Cerrar recordset y liberar conexión
        //5. Regresar arreglo de registros
        HashMap[] registros = new HashMap[0];
        String driver = _driver;

        try {
            try {
                Class.forName(driver);
            } catch (ClassNotFoundException cnfe) {
                System.out.println("No se localiza el driver de conexion a la base de datos");
            }

            String strConexion = _connectString;
            String strUser = _user;
            String strPassword = _password;
            Connection cnn = DriverManager.getConnection(strConexion, strUser, strPassword);

            Statement stmt = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs1 = stmt.executeQuery(strSQL);

            ResultSetMetaData rsMetaData = rs1.getMetaData();
            int numberOfColumns = rsMetaData.getColumnCount();

            rs1.last();
            int intTotalRegistros = rs1.getRow();
            rs1.beforeFirst();

            registros = new HashMap[intTotalRegistros];

            int intContador = 0;
            while (rs1.next()) {
                HashMap registro = new HashMap();
                for (int intColumna = 1; intColumna <= numberOfColumns; intColumna++) {
                    String strCampo = rsMetaData.getColumnName(intColumna);
                    if (rs1.getString(strCampo) == null) {
                        registro.put(strCampo, "");
                    } else {
                        registro.put(strCampo, rs1.getString(strCampo));
                    }
                }
                registros[intContador] = registro;
                intContador++;
            }
            stmt.close();
            cnn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return registros;

    }//QuerySQL

    public HashMap[] ExecuteSQL(String strSQL) {

        HSql();
        String debugmode = _debug;
        if (debugmode.equals("1")) {
            System.out.println(strSQL);
        }

        String strSuccess = "0";

        //1. Abrir conexión (reciclar de connection pool)
        //2. Ejecutar comando
        //3. Regresar arreglo con valor exito/fracaso
        String driver = _driver;

        try {
            try {
                Class.forName(driver);
            } catch (ClassNotFoundException cnfe) {
                System.out.println("No se localiza el driver de conexion a la base de datos");
            }

            String strConexion = _connectString;
            String strUser = _user;
            String strPassword = _password;
            Connection cnn = DriverManager.getConnection(strConexion, strUser, strPassword);

            Statement stmt = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            stmt.executeUpdate(strSQL);
            stmt.close();

            cnn.close();

            strSuccess = "1";

        } catch (Exception e) {
            e.printStackTrace();
        }

        int intTotalRegistros = 1;
        HashMap[] registros = new HashMap[intTotalRegistros];
        HashMap registro = new HashMap();
        registro.put("success", strSuccess);
        registros[0] = registro;

        return registros;

    }//ExecuteSQL

    public HashMap[] ExecuteSQLWithReturn(String strSQL, String strTable) {

        HSql();
        String debugmode = _debug;
        if (debugmode.equals("1")) {
            System.out.println(strSQL);
        }

        //1. Abrir conexión (reciclar de connection pool)
        //2. Realizar consulta
        //3. Recorrer recordset, creando un hashtable por registro
        //3.1 Cada campo será un elemento dentro de la tabla
        //4. Cerrar recordset y liberar conexión
        //5. Regresar arreglo de registros
        HashMap[] registros = new HashMap[0];
        String driver = _driver;

        try {
            try {
                Class.forName(driver);
            } catch (ClassNotFoundException cnfe) {
                System.out.println("No se localiza el driver de conexion a la base de datos");
            }

            String strConexion = _connectString;
            String strUser = _user;
            String strPassword = _password;
            Connection cnn = DriverManager.getConnection(strConexion, strUser, strPassword);

            Statement stmt = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            int intResult = stmt.executeUpdate(strSQL);

            ResultSet rs1 = stmt.executeQuery("select last_insert_id() as last_id from " + strTable);
            //lastid = rs.getString("last_id");

            //ResultSet rs1 = stmt.getGeneratedKeys();
            ResultSetMetaData rsMetaData = rs1.getMetaData();
            int numberOfColumns = rsMetaData.getColumnCount();

            rs1.last();
            int intTotalRegistros = rs1.getRow();
            rs1.beforeFirst();

            registros = new HashMap[intTotalRegistros];

            int intContador = 0;
            while (rs1.next()) {
                HashMap registro = new HashMap();
                for (int intColumna = 1; intColumna <= numberOfColumns; intColumna++) {
                    String strCampo = rsMetaData.getColumnName(intColumna);
                    if (rs1.getString(strCampo) == null) {
                        registro.put(strCampo, "");
                    } else {
                        registro.put(strCampo, rs1.getString(strCampo));
                    }
                }
                registros[intContador] = registro;
                intContador++;
            }
            stmt.close();
            cnn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return registros;

    }//ExecuteSQLWithReturn

}
