package Model;

/**
 * @author Zaid
 */
public class TituloModel {

    String no;
    String titulo;
    String subTitulo;
    String titulo2;
    String subTitulo2;
    String titulo3;
    String cotenido;
    String titulo4;
    String Cotenido2;
    String titulo5;
    String Cotenido3;
    String status;
    String update;

    public TituloModel() {

        this.no = "";
        this.titulo = "";
        this.subTitulo = "";
        this.titulo2 = "";
        this.subTitulo2 = "";
        this.titulo3 = "";
        this.cotenido = "";
        this.titulo4 = "";
        this.Cotenido2 = "";
        this.titulo5 = "";
        this.Cotenido3 = "";
        this.status = "";
        this.update = "";
    }

    public String getNo() {
        return no;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getSubTitulo() {
        return subTitulo;
    }

    public String getTitulo2() {
        return titulo2;
    }

    public String getSubTitulo2() {
        return subTitulo2;
    }

    public String getTitulo3() {
        return titulo3;
    }

    public String getCotenido() {
        return cotenido;
    }

    public String getTitulo4() {
        return titulo4;
    }

    public String getCotenido2() {
        return Cotenido2;
    }

    public String getTitulo5() {
        return titulo5;
    }

    public String getCotenido3() {
        return Cotenido3;
    }

    public String getStatus() {
        return status;
    }

    public String getUpdate() {
        return update;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setSubTitulo(String subTitulo) {
        this.subTitulo = subTitulo;
    }

    public void setTitulo2(String titulo2) {
        this.titulo2 = titulo2;
    }

    public void setSubTitulo2(String subTitulo2) {
        this.subTitulo2 = subTitulo2;
    }

    public void setTitulo3(String titulo3) {
        this.titulo3 = titulo3;
    }

    public void setCotenido(String cotenido) {
        this.cotenido = cotenido;
    }

    public void setTitulo4(String titulo4) {
        this.titulo4 = titulo4;
    }

    public void setCotenido2(String Cotenido2) {
        this.Cotenido2 = Cotenido2;
    }

    public void setTitulo5(String titulo5) {
        this.titulo5 = titulo5;
    }

    public void setCotenido3(String Cotenido3) {
        this.Cotenido3 = Cotenido3;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

}
