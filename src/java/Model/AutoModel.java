package Model;

/**
 * @author Ahorra_HP
 */
public class AutoModel {

    String idAuto;
    String imgAuto;
    String marcaAuto;
    String añoAuto;
    String modelAuto;
    String categAuto;
    String kmAuto;
    String transAuto;
    String precio;
    String statusAuto;
    String updateAuto;

    public AutoModel() {
        this.idAuto = "";
        this.imgAuto = "";
        this.marcaAuto = "";
        this.añoAuto = "";
        this.modelAuto = "";
        this.categAuto = "";
        this.kmAuto = "";
        this.transAuto = "";
        this.precio = "";
        this.statusAuto = "";
        this.updateAuto = "";
    }

    public String getIdAuto() {
        return idAuto;
    }

    public String getImgAuto() {
        return imgAuto;
    }

    public String getMarcaAuto() {
        return marcaAuto;
    }

    public String getAñoAuto() {
        return añoAuto;
    }

    public String getModelAuto() {
        return modelAuto;
    }

    public String getCategAuto() {
        return categAuto;
    }

    public String getKmAuto() {
        return kmAuto;
    }

    public String getTransAuto() {
        return transAuto;
    }

    public String getPrecio() {
        return precio;
    }

    public String getStatusAuto() {
        return statusAuto;
    }

    public String getUpdateAuto() {
        return updateAuto;
    }

    public void setIdAuto(String idAuto) {
        this.idAuto = idAuto;
    }

    public void setImgAuto(String imgAuto) {
        this.imgAuto = imgAuto;
    }

    public void setMarcaAuto(String marcaAuto) {
        this.marcaAuto = marcaAuto;
    }

    public void setAñoAuto(String añoAuto) {
        this.añoAuto = añoAuto;
    }

    public void setModelAuto(String modelAuto) {
        this.modelAuto = modelAuto;
    }

    public void setCategAuto(String categAuto) {
        this.categAuto = categAuto;
    }

    public void setKmAuto(String kmAuto) {
        this.kmAuto = kmAuto;
    }

    public void setTransAuto(String transAuto) {
        this.transAuto = transAuto;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public void setStatusAuto(String statusAuto) {
        this.statusAuto = statusAuto;
    }

    public void setUpdateAuto(String updateAuto) {
        this.updateAuto = updateAuto;
    }

}
