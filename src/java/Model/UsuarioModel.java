package Model;

/**
 * @author Ahorra_HP
 */
public class UsuarioModel {

    String idUsr;
    String nombreUsr;
    String correoUsr;
    String nipUsr;
    String privUsr;
    String statusUsr;
    String updateUsr;

    public UsuarioModel() {
        this.idUsr = "";
        this.nombreUsr = "";
        this.correoUsr = "";
        this.nipUsr = "";
        this.privUsr = "";
        this.statusUsr = "";
        this.updateUsr = "";
    }

    public String getIdUsr() {
        return idUsr;
    }

    public String getNombreUsr() {
        return nombreUsr;
    }

    public String getCorreoUsr() {
        return correoUsr;
    }

    public String getNipUsr() {
        return nipUsr;
    }

    public String getPrivUsr() {
        return privUsr;
    }

    public String getStatusUsr() {
        return statusUsr;
    }

    public String getUpdateUsr() {
        return updateUsr;
    }

    public void setIdUsr(String idUsr) {
        this.idUsr = idUsr;
    }

    public void setNombreUsr(String nombreUsr) {
        this.nombreUsr = nombreUsr;
    }

    public void setCorreoUsr(String correoUsr) {
        this.correoUsr = correoUsr;
    }

    public void setNipUsr(String nipUsr) {
        this.nipUsr = nipUsr;
    }

    public void setPrivUsr(String privUsr) {
        this.privUsr = privUsr;
    }

    public void setStatusUsr(String statusUsr) {
        this.statusUsr = statusUsr;
    }

    public void setUpdateUsr(String updateUsr) {
        this.updateUsr = updateUsr;
    }

}
