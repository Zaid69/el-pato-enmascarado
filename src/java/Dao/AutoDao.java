package Dao;

import Model.AutoModel;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import sql.HSql;
import utilities.Tools;

/**
 * @author Ahorra_HP
 */
public class AutoDao {

    //SOLO 1 AUTO
    public AutoModel getAutoById(String AutoId) {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM auto WHERE id_auto = '" + AutoId + "';";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        AutoModel autoModel = new AutoModel();
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            autoModel.setIdAuto(registro.get("id_auto").toString());
            autoModel.setImgAuto(registro.get("img_auto").toString());
            autoModel.setMarcaAuto(registro.get("marca_auto").toString());
            autoModel.setAñoAuto(registro.get("ano_auto").toString());
            autoModel.setModelAuto(registro.get("model_auto").toString());
            autoModel.setCategAuto(registro.get("categ_auto").toString());
            autoModel.setKmAuto(registro.get("km_auto").toString());
            autoModel.setTransAuto(registro.get("trans_auto").toString());
            autoModel.setPrecio(registro.get("precio").toString());
        }
        return autoModel;
    }

    //ARREGLO DE AUTOS
    public AutoModel[] getAutoModel() {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM auto;";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        AutoModel[] autoModels = new AutoModel[arrQuery.length];
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            AutoModel am = new AutoModel();
            am.setIdAuto(registro.get("id_auto").toString());
            am.setImgAuto(registro.get("img_auto").toString());
            am.setMarcaAuto(registro.get("marca_auto").toString());
            am.setAñoAuto(registro.get("ano_auto").toString());
            am.setModelAuto(registro.get("model_auto").toString());
            am.setCategAuto(registro.get("categ_auto").toString());
            am.setKmAuto(registro.get("km_auto").toString());
            am.setTransAuto(registro.get("trans_auto").toString());
            am.setPrecio(registro.get("precio").toString());
            autoModels[i] = am;
        }
        return autoModels;
    }

    //INSERT DE AUTOS NUEVO
    public String newAuto(AutoModel am) {
        HSql sql = new HSql();
        String idAuto = "";
        HashMap[] arrResultados;
        String strSQL = "INSERT INTO auto (`img_auto`,`marca_auto`,`ano_auto`,`model_auto`,`categ_auto`,`km_auto`,`trans_auto`,`precio`,`status_auto`,`update_auto`)"
                + "VALUE('','" + am.getMarcaAuto() + "'," + am.getAñoAuto() + ",'" + am.getModelAuto() + "','" + am.getCategAuto() + "'," + am.getKmAuto() + ",'" + am.getTransAuto() + "'," + am.getPrecio() + ",1,now());";
        arrResultados = sql.ExecuteSQLWithReturn(strSQL, "auto");
        HashMap resultado = (HashMap) arrResultados[0];
        idAuto = (String) resultado.get("last_id");
        return idAuto;
    }

    //ACTUALIZA DATOS DEL AUTO
    public boolean updateAuto(AutoModel am) {
        HSql sql = new HSql();
        String strSQL = "UPDATE auto SET marca_auto = '" + am.getMarcaAuto() + "', ano_auto = " + am.getAñoAuto() + ", model_auto = '" + am.getModelAuto() + "', categ_auto = '" + am.getCategAuto() + "', km_auto = " + am.getKmAuto() + ", trans_auto = '" + am.getTransAuto() + "', precio = " + am.getPrecio() + ", update_auto = now() WHERE id_auto = " + am.getIdAuto() + ";";
        return sql.ComandoConsola(strSQL);
    }

    //INSERT IMAGEN DEL AUTO
    public boolean img(String strIdr, String strFileUploaded, String strRepository, String strExt, String archivoBorrar) throws IOException {
        String strRutaImagen = "";
        String strSeparadorCarpeta = "\\";
        if (strFileUploaded.lastIndexOf("\\") > 0) {
            strSeparadorCarpeta = "\\";
        } else {
            strSeparadorCarpeta = "/";
        }
        boolean success = false;
        String repository = strRepository + strSeparadorCarpeta + "media";
        String strPicturesRepository = repository;
        strRutaImagen = strPicturesRepository;
        File f = new File(strRutaImagen);
        if (f.isDirectory() == false) {
            success = f.mkdir();
        }
        strRutaImagen = strPicturesRepository + strSeparadorCarpeta + "producto";
        f = new File(strRutaImagen);
        if (f.isDirectory() == false) {
            success = f.mkdir();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String strFecha = sdf.format(new Date());
        strRutaImagen = strRutaImagen + strSeparadorCarpeta + strIdr + "_" + strFecha + "." + strExt;
        String prod_foto = "media/producto/" + strIdr + "_" + strFecha + "." + strExt;
        File uploadedFile = new File(strFileUploaded);
        Tools.resizeImagenComplete(strFileUploaded, strRutaImagen, strExt, 95, 80);

        //borra el que subio en el post "upload"
        uploadedFile.delete();
        if (archivoBorrar.length() > 0) {
            uploadedFile = new File(archivoBorrar);
            uploadedFile.delete();
        }
        HSql sql = new HSql();
        String strSQL = "UPDATE auto SET ";
        strSQL = strSQL + " img_auto = '" + prod_foto + "',";
        strSQL = strSQL + " update_auto = now() ";
        strSQL = strSQL + " WHERE id_auto = " + strIdr;
        return sql.ComandoConsola(strSQL);
    }

    //BUSCA AUTO CON AJAX
    public AutoModel[] searchAuto(String Buscar) {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM auto WHERE (upper(marca_auto) LIKE upper('%"+Buscar+"%') OR upper(model_auto) LIKE upper('%"+Buscar+"%'));";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        AutoModel[] autoModels = new AutoModel[arrQuery.length];
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            AutoModel am = new AutoModel();
            am.setIdAuto(registro.get("id_auto").toString());
            am.setImgAuto(registro.get("img_auto").toString());
            am.setMarcaAuto(registro.get("marca_auto").toString());
            am.setAñoAuto(registro.get("ano_auto").toString());
            am.setModelAuto(registro.get("model_auto").toString());
            am.setCategAuto(registro.get("categ_auto").toString());
            am.setKmAuto(registro.get("km_auto").toString());
            am.setTransAuto(registro.get("trans_auto").toString());
            am.setPrecio(registro.get("precio").toString());
            autoModels[i] = am;
        }
        return autoModels;
    }
}