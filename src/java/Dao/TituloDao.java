package Dao;

import Model.TituloModel;
import java.util.HashMap;
import sql.HSql;

/**
 * @author Zaid
 */
public class TituloDao {

    //DAO PARA TRAER TODOS LOS DATOS DE VACE DE DATOS DEPENDIENDO DEL ID SOLO TRAE 1
    public TituloModel getTituloById(String tituloId) {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM txttitulo WHERE no = '" + tituloId + "';";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        TituloModel tituloModel = new TituloModel();
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            tituloModel.setNo(registro.get("no").toString());
            tituloModel.setTitulo(registro.get("titulo").toString());
            tituloModel.setSubTitulo(registro.get("subtitulo").toString());
            tituloModel.setTitulo2(registro.get("titulo_2").toString());
            tituloModel.setSubTitulo2(registro.get("subtitulo_2").toString());
            tituloModel.setTitulo3(registro.get("titulo_3").toString());
            tituloModel.setCotenido(registro.get("cotenido").toString());
            tituloModel.setTitulo4(registro.get("titulo_4").toString());
            tituloModel.setCotenido2(registro.get("Cotenido_2").toString());
            tituloModel.setTitulo5(registro.get("titulo_5").toString());
            tituloModel.setCotenido3(registro.get("Cotenido_3").toString());
        }
        return tituloModel;
    }

    //DAO QUE TRAE EN ARREGLO DE MODELOS PARA MOSTRAR EN UNA TABLA
    public TituloModel[] getTituloModel() {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM txttitulo;";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        TituloModel[] tituloModels = new TituloModel[arrQuery.length];
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            TituloModel tm = new TituloModel();
            tm.setNo(registro.get("no").toString());
            tm.setTitulo(registro.get("titulo").toString());
            tm.setSubTitulo(registro.get("subtitulo").toString());
            tm.setTitulo2(registro.get("titulo_2").toString());
            tm.setSubTitulo2(registro.get("subtitulo_2").toString());
            tm.setTitulo3(registro.get("titulo_3").toString());
            tm.setCotenido(registro.get("cotenido").toString());
            tm.setTitulo4(registro.get("titulo_4").toString());
            tm.setCotenido2(registro.get("Cotenido_2").toString());
            tm.setTitulo5(registro.get("titulo_5").toString());
            tm.setCotenido3(registro.get("Cotenido_3").toString());
            tituloModels[i] = tm;
        }
        return tituloModels;
    }
    //DAO PARA ACTUALIZAR UN TITULO
    public boolean updateTituloModel(TituloModel tm){
        boolean result = false;
        HSql sql = new HSql();
        String strSQL = "UPDATE txttitulo SET titulo = '"+tm.getTitulo()+"', subtitulo = '"+tm.getSubTitulo()+"', titulo_2 = '"+tm.getTitulo2()+"', subtitulo_2 = '"+tm.getSubTitulo2()+"', titulo_3 = '"+tm.getTitulo3()+"', cotenido = '"+tm.getCotenido()+"', titulo_4 = '"+tm.getTitulo4()+"', Cotenido_2 = '"+tm.getCotenido2()+"', titulo_5 = '"+tm.getTitulo5()+"', Cotenido_3 = '"+tm.getCotenido3()+"', update_titulo = now() WHERE no = '"+tm.getNo()+"';";
        result = sql.ComandoConsola(strSQL);
        return result;
    }
}
