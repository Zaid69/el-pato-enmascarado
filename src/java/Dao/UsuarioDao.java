package Dao;

import Model.UsuarioModel;
import java.util.HashMap;
import sql.HSql;

/**
 * @author Ahorra_HP
 */
public class UsuarioDao {

    public UsuarioModel getUsuario(UsuarioModel um) {
        HSql sql = new HSql();
        String strSQL = "SELECT * FROM usuario where correo_usr = '"+um.getCorreoUsr()+"' and nip_usr = '"+um.getNipUsr()+"' and status_usr = 1;";
        HashMap[] arrQuery = sql.QuerySQL(strSQL);
        UsuarioModel usuarioModel = new UsuarioModel();
        for (int i = 0; i < arrQuery.length; i++) {
            HashMap registro = (HashMap) arrQuery[i];
            usuarioModel.setIdUsr(registro.get("id_usr").toString());
            usuarioModel.setNombreUsr(registro.get("nombre_usr").toString());
            usuarioModel.setCorreoUsr(registro.get("correo_usr").toString());
            usuarioModel.setNipUsr(registro.get("nip_usr").toString());
            usuarioModel.setPrivUsr(registro.get("priv_usr").toString());
        }
        return usuarioModel;
    }

}
