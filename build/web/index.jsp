<%-- 
    Document   : index
    Created on : 31/08/2019, 07:41:53 AM
    Author     : Zaid
--%>
<%@page import="Controller.AutoController"%>
<%@page import="Model.AutoModel"%>
<%@page import="Controller.TituloController"%>
<%@page import="Model.TituloModel"%>
<%
    TituloModel tituloModels = new TituloController().getModelById("1");
    AutoModel[] AM = new AutoController().getAutoModels();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>El Pato Enmascarado</title>
        <link rel="icon" type="image/png" href="img/Pato.png">
        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="vendor/jquery/jquery.min.js"></script>
        <!--Estilos-->
        <link rel="stylesheet" href="css/style.css">
        <!--js-->
        <script src="js/imports.js" ></script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div id="Nav"></div>
                </div>


                <div class="col-12">
                    <!-- HOME -->
                    <div id="Home">
                        <h1><%=tituloModels.getTitulo()%></h1>
                        <p><%=tituloModels.getSubTitulo()%></p>
                    </div>
                    <!-- SEMINUEVOS -->
                    <div id="SemiNuevos">
                        <h2><%=tituloModels.getTitulo2()%></h2>
                        <p><%=tituloModels.getSubTitulo2()%></p>
                        <div class="row">
                            <%for (int cAu = 0; cAu < AM.length; cAu++) {
                                    AutoModel am = AM[cAu];
                                    if (am.getCategAuto().equals("SEMI NUEVO")) {
                            %>
                            <div class="col-3">
                                <div class="card">
                                    <img src="<%=am.getImgAuto()%>">
                                    <p>Marca: <b><%=am.getMarcaAuto()%></b></p>
                                    <p>Año: <b><%=am.getAñoAuto()%></b></p>
                                    <p>Modelo: <b><%=am.getModelAuto()%></b></p>
                                    <p>Kilometraje: <b><%=am.getKmAuto()%> Km</b></p>
                                    <p>Transmision: <b><%=am.getTransAuto()%></b></p>
                                    <p>Precio: <b><%=am.getPrecio()%></b></p>
                                </div>
                            </div>
                            <%}
                                }%>
                        </div>
                    </div>
                    <!-- USADOS -->
                    <div id="Usados">
                        <h2><%=tituloModels.getTitulo3()%></h2>
                        <p><%=tituloModels.getCotenido()%></p>
                        <div class="row">
                            <%for (int cAu = 0; cAu < AM.length; cAu++) {
                                    AutoModel am = AM[cAu];
                                    if (am.getCategAuto().equals("USADO")) {
                            %>
                            <div class="col-3">
                                <div class="card">
                                    <img src="<%=am.getImgAuto()%>">
                                    <p>Marca: <b><%=am.getMarcaAuto()%></b></p>
                                    <p>Año: <b><%=am.getAñoAuto()%></b></p>
                                    <p>Modelo: <b><%=am.getModelAuto()%></b></p>
                                    <p>Kilometraje: <b><%=am.getKmAuto()%> Km</b></p>
                                    <p>Transmision: <b><%=am.getTransAuto()%></b></p>
                                    <p>Precio: <b><%=am.getPrecio()%></b></p>
                                </div>
                            </div>
                            <%}
                                }%>
                        </div>
                    </div>
                    <!-- BARATOS -->
                    <div id="Baratos">
                        <h3><%=tituloModels.getTitulo4()%></h3>
                        <p><%=tituloModels.getCotenido2()%></p>
                        <div class="row">
                            <%for (int cAu = 0; cAu < AM.length; cAu++) {
                                    AutoModel am = AM[cAu];
                                    if (am.getCategAuto().equals("BARATO")) {
                            %>
                            <div class="col-3">
                                <div class="card">
                                    <img src="<%=am.getImgAuto()%>">
                                    <p>Marca: <b><%=am.getMarcaAuto()%></b></p>
                                    <p>Año: <b><%=am.getAñoAuto()%></b></p>
                                    <p>Modelo: <b><%=am.getModelAuto()%></b></p>
                                    <p>Kilometraje: <b><%=am.getKmAuto()%> Km</b></p>
                                    <p>Transmision: <b><%=am.getTransAuto()%></b></p>
                                    <p>Precio: <b><%=am.getPrecio()%></b></p>
                                </div>
                            </div>
                            <%}
                                }%>
                        </div>
                    </div>
                    <!-- DEPORTIVOS -->
                    <div id="Deportivos">
                        <h3><%=tituloModels.getTitulo5()%></h3>
                        <p><%=tituloModels.getCotenido3()%></p>
                        <div class="row">
                            <%for (int cAu = 0; cAu < AM.length; cAu++) {
                                    AutoModel am = AM[cAu];
                                    if (am.getCategAuto().equals("DEPORTIVO")) {
                            %>
                            <div class="col-3">
                                <div class="card">
                                    <img src="<%=am.getImgAuto()%>">
                                    <p>Marca: <b><%=am.getMarcaAuto()%></b></p>
                                    <p>Año: <b><%=am.getAñoAuto()%></b></p>
                                    <p>Modelo: <b><%=am.getModelAuto()%></b></p>
                                    <p>Kilometraje: <b><%=am.getKmAuto()%> Km</b></p>
                                    <p>Transmision: <b><%=am.getTransAuto()%></b></p>
                                    <p>Precio: <b><%=am.getPrecio()%></b></p>
                                </div>
                            </div>
                            <%}
                                }%>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>

</html>