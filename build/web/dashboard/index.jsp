<%-- 
    Document   : index
    Created on : 31/08/2019, 06:47:38 PM
    Author     : Zaid
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dashboard El Pato Enmascarado</title>
        <link rel="icon" type="image/png" href="img/Pato.png">
        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="vendor/jquery/jquery.min.js"></script>
        <!--Estilos-->
        <link rel="stylesheet" href="css/style.css">
        <!--js-->
        <script src="js/frm.js"></script>
    </head>

    <body>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="txtLogin">
                        <h1>Login</h1>
                        <form id="frmLogin" action="../UsuarioController" method="POST">
                            <div class="txtInp">
                                <input name="correo" type="text" required>
                                <span name="1"></span>
                                <span name="2"></span>
                                <label>Correo</label>
                            </div>
                            <div class="txtInp">
                                <input name="nip" type="password" required>
                                <span name="1"></span>
                                <span name="2"></span>
                                <label>Contraseña</label>
                            </div>
                            <div>
                                <p onclick="validaIngreso()" class="btn">Entrar<img src="../img/indicador.png" alt=">"></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>

</html>