-- 1.1 La integridad referencial es un sistema de reglas que utilizan la mayoría de las bases de datos relacionales para asegurarse que los registros de tablas relacionadas son válidos y que no se borren o cambien datos relacionados de forma accidental produciendo errores de integridad.
    /*
    Relación Uno a Uno
    Relación Uno a Varios
    Relación Varios a Varios
    */

-- 1.2 En relacion de tablas son tablas que no tienen ninguna relacion con otra tabala
    /*
    Explicación: nuestra tabla de proveedores tiene 9 proveedores de los productos de nuestra empresa, pero al observar la tabla de PEDIDOS vemos los pedidos 5, 7 y 8 cuyos números de proveedor (columna NP) son 0, 11 y 12 y es aquí donde se encuentra la inconsistencia. ¿Quienes son los proveedores 0, 11 y 12? Estos proveedores no existen y por tanto forman pedidos de productos inexistentes. Son registros absurdos, huerfanos o colgados.
    Los datos en rojo son registros huerfanos en la relación entre PROVEEDORES y PEDIDOS. Acces creará una relación indeterminada o se deberá desactivar la Intregidad referencial.
    */

-- 1.3 
    /*
    La instrucción MERGE está estructurada para manejar las tres operaciones, INSERT, UPDATE y DELETE, en un solo comando.
    Mientras que UPDATE solo modifica los valores de las columnas
    y el cursor solo afecta un campo
    */

-- 1.4 Una vista de base de datos es un subconjunto de una base de datos y se basa en una consulta que se ejecuta en una o más tablas de base de datos. Las vistas de base de datos se guardan en la base de datos como consultas con nombre y se pueden utilizar para guardar consultas completas que se utilizan con frecuencia.
    -- Ventajas:
    /*
    Facilita el manejo de grandes volúmenes de información, haciéndolos mas fáciles y rápidos de manipular.
    Brinda mayor seguridad a la información.
    Evita la redundancia de la información.
    Mejora la metodología de trabajo, se hace mas organizada.
    Facilita la realización de consultas en la BD por lo que se facilita la toma de decisiones.
    */
-- 1.5 Es un procedimiento que se ejecuta cuando se cumple una condición establecida al realizar una operación. Dependiendo de la base de datos, los triggerspueden ser de inserción (INSERT), actualización (UPDATE) o borrado (DELETE). Algunas bases de datos pueden ejecutar triggers al crear, borrar o editar usuarios, tablas, bases de datos u otros objetos
    -- Ventajas
    /*
    Ofrece chequeos de seguridad basada en valores.
    Restricciones dinámicasde integridad de datos y de integridad referencial.
    Asegura que las operaciones relacionadasse realizan juntas de forma implícita.
    Respuesta inmediata ante un evento auditado.
    Ofrece mayor control sobre la BD
    */
-- 1.6 PL/SQL (Procedural Language/Structured Query Language) es un lenguaje de programación incrustado en Oracle. PL/SQL soportará todas las consultas, ya que la manipulación de datos que se usa es la misma que en SQL,
    -- Ventajas
    /*          
    Facilita enormemente la gestión y la seguridad.
    Mejora notablemente el rendimiento al estar compilados y almacenados en la base de datos.
    Mejora la gestión de la memoria.
    Mejora y ayuda a tener mayor productividad y efectividad.
    */
2.1 RENAME TABLE PRODUCTO TO PRODUCTOS