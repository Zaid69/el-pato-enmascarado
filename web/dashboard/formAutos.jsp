<%-- 
    Document   : formAutos
    Created on : 4/09/2019, 11:16:36 AM
    Author     : Ahorra_HP
--%>
<%@page import="Controller.AutoController"%>
<%@page import="Model.AutoModel"%>
<%
    String autoId = request.getParameter("autoId");
    if (autoId == null) {
        autoId = "";
        System.out.println(autoId);
    } else {
        autoId = autoId;
    }
    AutoModel am = new AutoController().getAutoById(autoId);
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>El Pato Enmascarado</title>
        <link rel="icon" type="image/png" href="img/Pato.png">
        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="vendor/jquery/jquery.min.js"></script>
        <!--Estilos-->
        <link rel="stylesheet" href="css/style.css">
        <!-- js -->
        <script src="js/imports.js"></script>
        <!--<script src="js/general.js"></script>-->
        <!--<script src="js/frm.js"></script>-->
    </head>

    <body>
        <div id="menu"></div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-2">
                    <div id="menu"></div>
                </div>
                <div class="col-4"></div>
                <div class="col-6">
                    <div>
                        <div>
                            <p>Nuevo Auto</p>
                        </div>
                        <form id="frmAuto" method="POST" action="../AutoController" enctype="multipart/form-data">
                            <img src="../<%=am.getImgAuto()%>" width="60px"/>
                            <div class="box">
                                <input type="file" name="img" id="file-1" class="inputfile inputfile-1" 
                                       data-multiple-caption="{count} files selected" style="display: none;"/>
                                <label for="file-1" class="btn btn-info">
                                    <span>Selecciona Tu Imgen</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-file-image"></i>
                                </label>
                            </div>
                            <div>
                                <label>Marca</label>
                                <input name="marca" class="form-control" type="text" value="<%=am.getMarcaAuto()%>">
                            </div>
                            <div>
                                <label>Año</label>
                                <input name="ano" class="form-control" type="number" value="<%=am.getAñoAuto()%>">
                            </div>
                            <div>
                                <label>Modelo</label>
                                <input name="modelo" class="form-control" type="text" value="<%=am.getModelAuto()%>">
                            </div>
                            <div>
                                <label>Categoria</label>
                                <input name="categoria" class="form-control" type="text" value="<%=am.getCategAuto()%>">
                            </div>
                            <div>
                                <label>Kilometraje</label>
                                <input name="km" class="form-control" type="number" value="<%=am.getKmAuto()%>">
                            </div>
                            <div>
                                <label>Transmision</label>
                                <input name="transmision" class="form-control" type="text" value="<%=am.getTransAuto()%>">
                            </div>
                            <div>
                                <label>Precio</label>
                                <input name="precio" class="form-control" type="number" value="<%=am.getPrecio()%>">
                            </div>
                            <div>
                                <button class="btn btn-danger">
                                    <a href="menu.jsp">Cancelar</a>
                                </button>
                                <input type="hidden" name="idAuto" value="<%=am.getIdAuto()%>">
                                <% if (autoId.equals("")) { %>
                                <input type="hidden" name="Mary" value="nuevoAuto">
                                <%}else{%>
                                <input type="hidden" name="Mary" value="EditAuto">
                                <%}%>
                                <button class="btn btn-success">
                                    Guardar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>

</html>