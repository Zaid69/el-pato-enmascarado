<%-- 
    Document   : listTitulo
    Created on : 4/09/2019, 09:09:51 AM
    Author     : Ahorra_HP
--%>

<%@page import="Controller.TituloController"%>
<%@page import="Model.TituloModel"%>
<%
    TituloModel[] TM = new TituloController().getTituloModels();
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>El Pato Enmascarado</title>
        <link rel="icon" type="image/png" href="img/Pato.png">
        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="vendor/jquery/jquery.min.js"></script>
        <!--Estilos-->
        <link rel="stylesheet" href="css/style.css">
        <!-- js -->
        <script src="js/imports.js"></script>
        <script src="js/general.js"></script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div id="menu"></div>
                </div>
                <div class="col-12">
                    <table class="table" id="tbTitulo">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Titulo</th>
                                <td>SubTitulo</td>
                                <td>Titulo 2</td>
                                <td>SubTitulo 2</td>
                                <td>Titulo 3</td>
                                <td>Contenido</td>
                                <td>Titulo 4</td>
                                <td>Contenido 2</td>
                                <td>Titulo 5</td>
                                <td>Contenido 3</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%for (int tCon = 0; tCon < TM.length; tCon++) {
                                    TituloModel tm = TM[tCon];
                            %>
                            <tr data-id="<%=tm.getNo()%>">
                                <td><%=tm.getNo()%></td>
                                <td><%=tm.getTitulo()%></td>
                                <td><%=tm.getSubTitulo()%></td>
                                <td><%=tm.getTitulo2()%></td>
                                <td><%=tm.getSubTitulo2()%></td>
                                <td><%=tm.getTitulo3()%></td>
                                <td><%=tm.getCotenido()%></td>
                                <td><%=tm.getTitulo4()%></td>
                                <td><%=tm.getCotenido2()%></td>
                                <td><%=tm.getTitulo5()%></td>
                                <td><%=tm.getCotenido3()%></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>

</html>