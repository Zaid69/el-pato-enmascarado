<%-- 
    Document   : menu
    Created on : 3/09/2019, 10:44:18 AM
    Author     : Ahorra_HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>El Pato Enmascarado</title>
    <link rel="icon" type="image/png" href="img/Pato.png">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="vendor/jquery/jquery.min.js"></script>
    <!--Estilos-->
    <link rel="stylesheet" href="css/style.css">
    <!-- js -->
    <script src="js/imports.js"></script>
</head>

<body>
    <div id="menu"></div>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>