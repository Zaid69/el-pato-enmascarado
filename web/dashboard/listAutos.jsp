<%-- 
    Document   : listAuto
    Created on : 5/09/2019, 12:14:04 PM
    Author     : Ahorra_HP
--%>
<%@page import="Controller.AutoController"%>
<%@page import="Model.AutoModel"%>
<%
    AutoModel[] AM = new AutoController().getAutoModels();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>El Pato Enmascarado</title>
        <link rel="icon" type="image/png" href="img/Pato.png">
        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="vendor/jquery/jquery.min.js"></script>
        <!--Estilos-->
        <link rel="stylesheet" href="css/style.css">
        <!-- js -->
        <script src="js/imports.js"></script>
        <script src="js/general.js"></script>
        <script src="js/autos.js"></script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div id="menu"></div>
                </div>
                <div class="col-9 offset-1">
                    <input type="text" id="txtSearch" name="txtSearch" 
                           placeholder="Marca o Modelo del Auto" class="form-control">
                </div>
                <div class="col-1">
                    <button type="reset" class="btn btn-success" onclick="buscaAuto()">
                        <i class="fa fa-search"></i> Buscar
                    </button>
                </div>
                <div class="col-12 mt-5">
                    <table class="table" id="tbAuto">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Imagen</th>
                                <th>Marca</th>
                                <th>Año</th>
                                <th>Modelo</th>
                                <th>Categoria</th>
                                <th>Kilometraje</th>
                                <th>Transmision</th>
                                <th>Precio</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for (int cAu = 0; cAu < AM.length; cAu++) {
                                    AutoModel am = AM[cAu];
                            %>
                            <tr data-id="<%=am.getIdAuto()%>">
                                <td><%=am.getIdAuto()%></td>
                                <td><img src="../<%=am.getImgAuto()%>"></td>
                                <td><%=am.getMarcaAuto()%></td>
                                <td><%=am.getAñoAuto()%></td>
                                <td><%=am.getModelAuto()%></td>
                                <td><%=am.getCategAuto()%></td>
                                <td><%=am.getKmAuto()%> KM</td>
                                <td><%=am.getTransAuto()%></td>
                                <td>$ <%=am.getPrecio()%></td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
