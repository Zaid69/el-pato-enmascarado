function buscaAuto() {
    //armamos la cadena para la busqueda, con el tipo 1 y lo que escribieron 
    //en el input
    let cadena = "1|" + $('#txtSearch').val();
    //peticion por ajax
    jQuery.ajax({
        type: "POST",
        url: "../AutoController",
        data: "loadSearch=" + cadena,
        success: function (data) {
            //removemos las filas existentes
            $("#tbAuto").find("tr:gt(0)").remove();
            //Se realiza el casting para la respuesta
            let dataObject = jQuery.parseJSON(data);
            //version simplificada del loop (for)
            dataObject.autos.forEach(function (autos) {
                let fila = "<tr class='celda' data-id='" + autos.id + "'> " +
                        "<td>" + autos.id + "</td>" +
                        "<td> <img src='../" + autos.img + "'width='50px'> </td>" +
                        "<td>" + autos.marca + "</td>" +
                        "<td>" + autos.ano + "</td>" +
                        "<td>" + autos.modelo + "</td>" +
                        "<td>" + autos.cat + "</td>" +
                        "<td>" + autos.km + "</td>" +
                        "<td>" + autos.trans + "</td>" +
                        "<td>" + autos.precio + "</td>" +
                        "</tr>";
                //se agrega la fila a la tabla, despues de la ultima fila q contenga
                $('#tbAuto tr:last').after('' + fila);
            });
        },
        error: function (data) {
            $('#error').show();
        }
    });
}