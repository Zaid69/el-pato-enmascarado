//FUNCION PARA CLICK DE LA TABLA
function clickTableT() {
    $('#tbTitulo td').click(function () {
        $("body").css("cursor", "progress");
        var idTitulo = $(this).parent().attr('data-id');
        post('formTitulo.jsp',
                {tituloId: idTitulo});
    });
}
//FUNCION QUE MANDA A LLAMAR LA FUNCION ANTERIOR
$(function () {
    clickTableT();
});
//FUNCION PARA CLICK DE LA TABLA
function clickTableA() {
    $('#tbAuto td').click(function () {
        $("body").css("cursor", "progress");
        var idAuto = $(this).parent().attr('data-id');
        post('formAutos.jsp',
                {autoId: idAuto});
    });
}
//FUNCION QUE MANDA A LLAMAR LA FUNCION ANTERIOR
$(function () {
    clickTableA();
});

function post(path, params, method) {
    method = method || "post";

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    form.setAttribute("charset", "UTF-8");

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}