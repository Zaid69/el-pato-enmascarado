<%-- 
    Document   : formTitulo
    Created on : 4/09/2019, 09:09:00 AM
    Author     : Ahorra_HP
--%>
<%@page import="Controller.TituloController"%>
<%@page import="Model.TituloModel"%>
<%
    String TituloModelId = request.getParameter("tituloId");
    TituloModel tm = new TituloController().getModelById(TituloModelId);
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>El Pato Enmascarado</title>
        <link rel="icon" type="image/png" href="img/Pato.png">
        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="vendor/jquery/jquery.min.js"></script>
        <!--Estilos-->
        <link rel="stylesheet" href="css/style.css">
        <!-- js -->
        <script src="js/imports.js"></script>
        <script src="js/general.js"></script>
    </head>

    <body>

        <div class="container-fluid">
            <div class="row">
                <div class="col-2">
                    <div id="menu"></div>
                </div>
                <div class="col-4"></div>
                <div class="col-6">
                    <div>
                        <form id="frmTitulo" method="POST" action="../TituloController">
                            <div>
                                <label>Titulo Principal</label>
                                <input name="Titulo" class="form-control" type="text" value="<%=tm.getTitulo()%>">
                            </div>
                            <div>
                                <label>Sub Titulo Principal</label>
                                <input name="SubTitulo" class="form-control" type="text" value="<%=tm.getSubTitulo()%>">
                            </div>
                            <div>
                                <label>Titulo 2</label>
                                <input name="Titulo2" class="form-control" type="text" value="<%=tm.getTitulo2()%>">
                            </div>
                            <div>
                                <label>Sub Titulo 2</label>
                                <input name="SubTitulo2" class="form-control" type="text" value="<%=tm.getSubTitulo2()%>">
                            </div>
                            <div>
                                <label>Titulo 3</label>
                                <input name="Titulo3" class="form-control" type="text" value="<%=tm.getTitulo3()%>">
                            </div>
                            <div>
                                <label>Contenido</label>
                                <input name="Contenido" class="form-control" type="text" value="<%=tm.getCotenido()%>">
                            </div>
                            <div>
                                <label>Titulo 4</label>
                                <input name="Titulo4" class="form-control" type="text" value="<%=tm.getTitulo4()%>">
                            </div>
                            <div>
                                <label>Contenido</label>
                                <input name="Contenido2" class="form-control" type="text" value="<%=tm.getCotenido2()%>">
                            </div>
                            <div>
                                <label>Titulo 5</label>
                                <input name="Titulo5" class="form-control" type="text" value="<%=tm.getTitulo5()%>">
                            </div>
                            <div>
                                <label>Contenido</label>
                                <input name="Contenido3" class="form-control" type="text" value="<%=tm.getCotenido3()%>">
                            </div>
                            <div>
                                <button class="btn btn-danger">
                                    <a href="menu.jsp">Cancelar</a>
                                </button>
                                <input type="hidden" name="tituloId" value="<%=tm.getNo()%>">
                                <input type="hidden" name="Mary" value="tituloEdit">
                                <button class="btn btn-success" onclick="Titulo()">
                                    Guardar Cambios
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>

</html>