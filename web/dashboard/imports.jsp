<%-- 
    Document   : imports
    Created on : 4/09/2019, 11:19:02 AM
    Author     : Ahorra_HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container-fluid" id="MenuD">
            <div class="row">
                <div class="col-12">
                    <div class="menu">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Menu">
                            <img src="../img/Menu.jpg" alt="Menu">
                        </button>
                        <div class="navbar collapse" id="Menu">
                            <ul class="nav flex-column">
                                <li>
                                    <button class="navbar-toggler">
                                        <a href="../index.jsp">Inicio</a>
                                    </button>
                                </li>
                                <li>
                                    <button class="navbar-toggler">
                                        <a href="menu.jsp">Menu</a>
                                    </button>
                                </li>
                                <li>
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#SubT">
                                        <span>Titulo</span>
                                    </button>

                                    <div class="navbar collapse" id="SubT">
                                        <ul class="nav flex-column">
                                            <li>
                                                <a href="listTitulo.jsp">Listado</a>
                                            </li>
                                            <li>
                                                <a href="formTitulo.jsp">Nuevo</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#SubA">
                                        <span>Autos</span>
                                    </button>

                                    <div class="navbar collapse" id="SubA">
                                        <ul class="nav flex-column">
                                            <li>
                                                <a href="listAutos.jsp">Listado</a>
                                            </li>
                                            <li>
                                                <a href="formAutos.jsp">Nuevo</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <button class="navbar-toggler">
                                        <span>Cerrar Sesion</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
